package com.harrilainio.weather1;

import com.harrilainio.weather1.json.Weather;
import com.harrilainio.weather1.json.WeatherAPI;
import com.harrilainio.weather1.json.WeatherRoot;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class OpenWeatherMapClient {

	private WebClient client() {
		return WebClient.create("http://api.openweathermap.org");
	}

	public WeatherAPI get(double lat, double lon ) {
		String uri = String.format(
				"/data/2.5/weather" +
						"?lat=%f&lon=%f&" +
						"mode=json&units=metric&" +
						"APPID=973c8627f865f21cb1a651160c0b920f&" +
						"lang=en",
				lat, lon);
		return client().get().uri(uri)
				.retrieve()
				.bodyToMono(WeatherRoot.class)
				.block()
				.transform();
	}

	public WeatherAPI test_get() {
		return WeatherRoot.createTest().transform();
	}
}
