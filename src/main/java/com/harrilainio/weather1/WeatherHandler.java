package com.harrilainio.weather1;

import com.harrilainio.weather1.json.WeatherAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Component
public class WeatherHandler {

	@Autowired
	private OpenWeatherMapClient openWeatherMapClient;

	public Mono<WeatherAPI> get(double lat, double lon) {
		return Mono.justOrEmpty(openWeatherMapClient.get( lat, lon ));
	}

	public Mono<WeatherAPI> test_get(/*ServerRequest request*/) {
		//
		// Oulu 65.02444156,25.5294914
		//
		return Mono.justOrEmpty(openWeatherMapClient.test_get());
	}

}
