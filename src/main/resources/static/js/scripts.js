var map;
var lat = document.getElementById('latcoords');
var lng = document.getElementById('loncoords');
var temp = document.getElementById('temp');
var city = document.getElementById('city');
var description = document.getElementById('description');
var pressure = document.getElementById('pressure');
var humidity = document.getElementById('humidity');
var method = document.getElementById('method');

function loadMap() {
    
    var mapOptions = {
        
        zoom: 11,
        
        center: new google.maps.LatLng(65.024, 25.529)
    };
    
    var mapid = document.getElementById('map');
    
    map = new google.maps.Map(mapid, mapOptions);

    //Add the event listeners 
    mapEventListeners();   
}

// Add the map event listeners
function mapEventListeners() {
    
    var mouseClick = google.maps.event.addListener(map, 'click',
        function(event) {

            //alert('Right lick!');
            updateCurrentLatLng(event.latLng);
            //temp.innerHTML = "testing  "
            UserAction(event.latLng);
        }
    );
      
}

function updateCurrentLatLng(latLng) {

    //Update the coordinates
    lat.innerHTML = latLng.lat().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    lng.innerHTML = latLng.lng().toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});
    
}

function UserAction(latLng) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:9000/weather?lat=" + latLng.lat() + "&lon=" + latLng.lng(), true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                //console.log(xhr.responseText);
                var json = JSON.parse(xhr.responseText);
                temp.innerHTML = json.temp;
                city.innerHTML = json.city;
                description.innerHTML = json.description;
                pressure.innerHTML = json.pressure;
                humidity.innerHTML = json.humidity;
                method.innerHTML = json.method;
            } else {
                console.error(xhr.statusText);
            }
        }
    };
    xhr.onerror = function (e) {
        console.error(xhr.statusText);
    };
    xhr.send(null);
}

google.maps.event.addDomListener(window, 'load', loadMap());