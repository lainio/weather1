package com.harrilainio.weather1;

import com.harrilainio.weather1.json.WeatherAPI;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ServletHttpHandlerAdapter;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.HashMap;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.resources;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@SpringBootApplication
@RestController
public class Weather1Application {

	@Autowired
	private WeatherHandler weatherHandler;

	private static Logger logger = LoggerFactory.getLogger(Weather1Application.class);

	@GetMapping(value = "/weather")
	public Mono<WeatherAPI> weather(
			@RequestParam(value="lat", defaultValue="0") double lat,
			@RequestParam(value="lon", defaultValue="0") double lon ) {
		return weatherHandler.get(lat, lon);
	}
//	public Mono<WeatherAPI> weather(
//			@RequestParam(value="lat", defaultValue="0") String lat,
//			@RequestParam(value="lon", defaultValue="0") String lon ) {
//		return weatherHandler.get(Double.parseDouble(lat), Double.parseDouble(lon));
//	}

	@GetMapping(value = "/ping")
	public Mono<String> ping() {
		return Mono.just("works!");
	}

	public static void main(String... args) throws Exception {
		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(Weather1Application.class)
				.properties(
						// reserve for future settings
						new HashMap<String, Object>() {{
							put("server.port", "9000");
						}}
				).run(args);
		assert (ctx != null);

		logger.info("Application started...");

		System.in.read();
		ctx.close();
	}

}
