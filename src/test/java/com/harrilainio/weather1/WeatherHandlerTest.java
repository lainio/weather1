package com.harrilainio.weather1;

import com.harrilainio.weather1.json.WeatherAPI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WeatherHandlerTest {

	private static WebTestClient client;

	@Value("${local.server.port}")
	private int port;
	private String uriBase;

	@Before
	public void setUp() throws Exception {
		uriBase = "http://localhost:" + port;
		client = WebTestClient
				.bindToServer()
				.baseUrl(uriBase)
				.build();
	}

	@Test
	public void getTestx_NotFound() throws Exception {
		client.get().uri("/testx").exchange().expectStatus().isNotFound();
	}

	@Test
	public void getWeather_test() {
		WebClient webClient = WebClient.create(uriBase);
		String uri = "/weather?lat=65.02444156&lon=25.5294914";
		WeatherAPI weatherAPI = webClient.get().uri(uri)
				.retrieve()
				.bodyToMono(WeatherAPI.class)
				.block();

		assertThat(weatherAPI.getCity(), equalTo("Oulu"));
	}
}
