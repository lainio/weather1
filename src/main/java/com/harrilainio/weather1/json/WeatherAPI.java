package com.harrilainio.weather1.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherAPI {
	private String city;
	private String method;
	private int timestamp;
	private int temp;
	private int pressure;
	private int humidity;
	private String description;
	private String icon_id;
}
