package com.harrilainio.weather1.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Main {
	private int temp;
	private int pressure;
	private int humidity;
	private int temp_min;
	private int temp_max;
}
