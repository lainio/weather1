package com.harrilainio.weather1.json;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class WeatherRootTest {
	private WeatherRoot weatherRoot = null;

	@Before
	public void setUp() throws Exception {
		weatherRoot = WeatherRoot.createTest();
	}

	@Test
	public void getName() {
		assertThat(weatherRoot.getName(), equalTo("Oulu"));
	}

	@Test
	public void getBase() {
		assertThat(weatherRoot.getBase(), equalTo("stations"));
	}

	@Test
	public void getMain() {
		Main main = weatherRoot.getMain();

		assertThat(main.getHumidity(), is(97));
		assertThat(main.getPressure(), is(996));
		assertThat(main.getTemp(), is(1));
		assertThat(main.getTemp_max(), is(1));
		assertThat(main.getTemp_min(), is(1));
	}

	@Test
	public void getWeather() {
		Weather weather = weatherRoot.getWeather()[0];

		assertThat(weather.getIcon(), equalTo("10n"));
		assertThat(weather.getDescription(), is("light rain"));
	}

	@Test
	public void transform() {
		WeatherAPI weatherAPI = weatherRoot.transform();

		assertThat(weatherAPI.getCity(), equalTo("Oulu"));
		assertThat(weatherAPI.getMethod(), equalTo("stations"));
		assertThat(weatherAPI.getIcon_id(), equalTo("http://openweathermap.org/img/w/10n.png"));
		assertThat(weatherAPI.getDescription(), equalTo("light rain"));
		assertThat(weatherAPI.getTimestamp(), is(1514562600));
		assertThat(weatherAPI.getTemp(), is(1));
		assertThat(weatherAPI.getPressure(), is(996));
		assertThat(weatherAPI.getHumidity(), is(97));
	}
}