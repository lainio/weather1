package com.harrilainio.weather1.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherRoot {
	private String name;
	private String base;
	private int dt;

	private Main main;
	private Weather[] weather;

	public WeatherAPI transform() {
		return new WeatherAPI( name, base, dt, main.getTemp(), main.getPressure(), main.getHumidity(),
				weather[0].getDescription(),
				"http://openweathermap.org/img/w/" + weather[0].getIcon() + ".png");
	}

	public static WeatherRoot createTest() {
		WeatherRoot weatherRoot = null;
		String fileName = "reply.json";

		try {
			FileReader fileReader = new FileReader(fileName);

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			StringBuilder stringBuilder = new StringBuilder();
			String line = null;

			while((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}

			bufferedReader.close();

			ObjectMapper objectMapper = new ObjectMapper();
			weatherRoot = objectMapper.readValue(stringBuilder.toString(), WeatherRoot.class);
		}
		catch(FileNotFoundException ex) {
			System.out.println("Unable to open file '" + fileName + "'");
		}
		catch(IOException ex) {
			System.out.println("Error reading file '" + fileName + "'");
		}
		return weatherRoot;
	}
}

